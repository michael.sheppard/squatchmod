package net.squatchmod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import net.squatchmod.common.SquatchMod;

@Environment(EnvType.CLIENT)
public class SquatchModClient implements ClientModInitializer {
    public static final EntityModelLayer squatchModelLayer = new EntityModelLayer(new Identifier(SquatchMod.MODID, "textures/entity/sasquatch/squatch.png"), SquatchMod.SQUATCH_NAME);

    @Override
    public void onInitializeClient() {
        SquatchMod.LOGGER.info(SquatchMod.MODID + "Client: onInitializeClient");
        EntityModelLayerRegistry.registerModelLayer(squatchModelLayer, SquatchModEntityModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SquatchMod.SQUATCH, SquatchModEntityRenderer::new);
    }
}
