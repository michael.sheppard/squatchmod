package net.squatchmod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.logging.Logger;

@SuppressWarnings("unused")
public class SquatchMod implements ModInitializer {
    public final static String MODID = "squatchmod";
    public final static String SQUATCH_NAME = "sasquatch";
    public final static String SQUATCH_SPAWN_EGG = "sasquatch_spawn_egg";
    public final static String SQUATCH_VOCAL_SOUND_NAME = "squatch.vocal";

    public final static Identifier SQUATCH_VOCAL_SOUND_ID = new Identifier(SquatchMod.MODID, SquatchMod.SQUATCH_VOCAL_SOUND_NAME);
    public static final SoundEvent SQUATCH_VOCAL_SOUND = new SoundEvent(SQUATCH_VOCAL_SOUND_ID);

    public static final Logger LOGGER = Logger.getLogger(SquatchMod.MODID);

    public static final EntityType<SquatchModEntity> SQUATCH = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MODID, SQUATCH_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, SquatchModEntity::new).dimensions(EntityDimensions.fixed(0.6f, 1.95f))
                    .trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    @Override
    public void onInitialize() {
        LOGGER.info(SquatchMod.MODID + ": onInitialize");
        FabricDefaultAttributeRegistry.register(SquatchMod.SQUATCH, SquatchModEntity.createAttributes());
        Registry.register(Registry.ITEM, new Identifier(SquatchMod.MODID, SquatchMod.SQUATCH_SPAWN_EGG),
                new SpawnEggItem(SquatchMod.SQUATCH, 0x8B4513, 0x8B4C39, new Item.Settings().group(ItemGroup.MISC)));
    }
}
