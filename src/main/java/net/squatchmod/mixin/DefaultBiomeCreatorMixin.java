package net.squatchmod.mixin;

import net.minecraft.entity.SpawnGroup;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.DefaultBiomeCreator;
import net.minecraft.world.biome.SpawnSettings;
import net.squatchmod.common.SquatchMod;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DefaultBiomeCreator.class)
public abstract class DefaultBiomeCreatorMixin {
    @Inject(at = @At("TAIL"), method = "createGiantTreeTaiga")
    private static void addSpawnGiantTreeTaiga(float depth, float scale, float temperature, boolean spruce, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createBirchForest")
    private static void addSpawnBirchForest(float depth, float scale, boolean tallTrees, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createBambooJungle")
    private static void addSpawnBambooJungle(float depth, float scale, int parrotWeight, int parrotMaxGroupSize, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createPlains")
    private static void createPlains(boolean sunflower, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createForest")
    private static void createForest(float depth, float scale, boolean flower, SpawnSettings.Builder spawnSettings, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createNormalForest")
    private static void createNormalForest(float depth, float scale, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createFlowerForest")
    private static void createFlowerForest(CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createTaiga")
    private static void createTaiga(float depth, float scale, boolean snowy, boolean mountains, boolean villages, boolean igloos, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createDarkForest")
    private static void createDarkForest(float depth, float scale, boolean hills, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }

    @Inject(at = @At("TAIL"), method = "createSwamp")
    private static void createSwamp(float depth, float scale, boolean hills, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(SquatchMod.SQUATCH, 10, 1, 2));
    }
}
