package net.squatchmod.client;

import net.minecraft.client.render.entity.BipedEntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.squatchmod.common.SquatchMod;
import net.squatchmod.common.SquatchModEntity;

public class SquatchModEntityRenderer extends BipedEntityRenderer<SquatchModEntity, SquatchModEntityModel<SquatchModEntity>> {
    public static final Identifier SQUATCH_TEXTURE = new Identifier(SquatchMod.MODID, "textures/entity/sasquatch/squatch.png");
    private static final float SCALE_FACTOR = 1.3f;

    public SquatchModEntityRenderer(EntityRendererFactory.Context ctx) {
        super(ctx, new SquatchModEntityModel<>(ctx.getPart(SquatchModClient.squatchModelLayer)), 0.05f);
    }

    public Identifier getTexture(SquatchModEntity entity) {
        return SQUATCH_TEXTURE;
    }

    protected void scale(SquatchModEntity squatchModEntity, MatrixStack matrixStack, float partialTickTime) {
        matrixStack.scale(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
        super.scale(squatchModEntity, matrixStack, partialTickTime);
    }
}
