package net.squatchmod.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.function.Predicate;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class SquatchModEntity extends PathAwareEntity {
    private static final Predicate<Entity> NOTICEABLE_PLAYER_FILTER;

    protected SquatchModEntity(EntityType<? extends PathAwareEntity> entityType, World world) {
        super(entityType, world);
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.25)
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 8.0D);
    }

    @Override
    protected void initGoals() {
        goalSelector.add(1, new SwimGoal(this));
        goalSelector.add(2, new WanderAroundFarGoal(this, 1.0D));
        goalSelector.add(3, new LookAtEntityGoal(this, PlayerEntity.class, 16.0F));
        goalSelector.add(4, new LookAroundGoal(this));
        goalSelector.add(5, new FleeEntityGoal<>(this, PlayerEntity.class, 16.0F, 1.6D, 1.4D, NOTICEABLE_PLAYER_FILTER::test));
    }

    @SuppressWarnings("unused")
    // implementing Biome specific spawning here
    public static boolean canSpawn(EntityType<SquatchModEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
//        boolean allowSpawn = validSpawnBiomes(world, pos);
//        SquatchMod.LOGGER.info(SquatchMod.MODID + "Entity::canSpawn -> " + allowSpawn);
//        return allowSpawn;
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.getBiomeKey(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BAMBOO_JUNGLE)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BAMBOO_JUNGLE_HILLS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST_HILLS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DARK_FOREST_HILLS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DARK_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_SPRUCE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SWAMP)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FLOWER_FOREST));
    }

    @Override
    public EntityType<?> getType() {
        return SquatchMod.SQUATCH;
    }

    @Override
    public boolean cannotDespawn() {
        return true;
    }

    @Override
    public EntityDimensions getDimensions(EntityPose p) {
        return new EntityDimensions(1.0f, 2.5f, false);
    }

    @Override
    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.9F;
    }

//    @Override
//    protected SoundEvent getAmbientSound() {
//        return SquatchMod.SQUATCH_VOCAL_SOUND;
//    }

    @Override
    public void playAmbientSound() {
        float pitch = 0.90f;
        playSound(SquatchMod.SQUATCH_VOCAL_SOUND, getSoundVolume(), pitch);
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
        playSound(getStepSound(), 0.15f, 0.5f);
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_ZOMBIE_STEP;
    }

    @Override
    protected float getSoundVolume() {
        return 0.15F;
    }

    static {
        NOTICEABLE_PLAYER_FILTER = (entity) -> !entity.isSneaky() && EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR.test(entity);
    }
}
